<?php

namespace Modules\EmailScheduleModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
 use Modules\EmailScheduleModule\Entities\EmailSchedule as iEmailSchedule;
use Illuminate\Support\Facades\Storage;
 use Modules\EmailManageModule\Entities\UserBatch as gUserBatch;
 use Modules\EmailManageModule\Entities\BatchImports as gBatchImports;
 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class EmailScheduleModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
	 
	public function __construct()
	{
			$this->middleware('auth');
	}
		
    public function index()
    {
		
				$getUserID=auth()->user()->id;
		    $data = gUserBatch::where('user_id',$getUserID)
               ->orderBy('id', 'desc')
                ->get();

        return view('emailschedulemodule::index')->withData($data);
		
     }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('emailschedulemodule::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
		 
		
  		$validatedData = $request->validate([
 
			'email_alias'  => 'required',
			'email_subject'  => 'required',
			'email_body'  => 'required|max:5000',
 			'date_s'  => 'required',
 		 	'attachment'  => 'max:32000',
			'user_batches_id'  => 'required',

		]);
		
			$date_s = \Carbon\Carbon::parse($request->input('date_s'));

			$request->input('name');
		 	$EmailSchedule = new iEmailSchedule;
		 	$EmailSchedule->user_id = auth()->user()->id;
		 	$EmailSchedule->email_alias = $request->input('email_alias');
		 	$EmailSchedule->email_subject = $request->input('email_subject');
		 	$EmailSchedule->email_body = $request->input('email_body');
			
			
 			if(!is_null($request->attachment)){
			$fileName = now()->format('Y-m-d-H-i-s');
			$uploadName=$fileName.$request->attachment->getClientOriginalName();
			$request->attachment->storeAs('attachment',$uploadName);
		  	$EmailSchedule->attachment = $uploadName;				
				
			} else {
				
		 	$EmailSchedule->attachment = $request->attachment;
				
				
			}

 	
		 	$EmailSchedule->date_s = $date_s->format('Y-m-d');
		 	$EmailSchedule->user_batches_id = $request->input('user_batches_id');
		 	$EmailSchedule->defaultstatusis =1;
		 	$EmailSchedule->save();
			
			if($EmailSchedule){
				
		 
				 //	 foreach ($gBatchImportsData as $row) {

				$gBatchImportsData = gBatchImports::where('user_batches_id',$EmailSchedule->user_batches_id)
				->orderBy('id', 'asc')
                ->get();
				
				
				$mail= new PHPMailer(true); // create a n			 
 				$body = $EmailSchedule->email_body;
				$mail->msgHTML($body);
				$mail->isSMTP();
				$mail->Host = 'smtp.gmail.com';												 
				$mail->SMTPAuth = true;
				$mail->SMTPKeepAlive = true;  
				$mail->Port = 587;                                     
				$mail->Username = "test@gmail.com";
				$mail->Password = "password";
				$mail->setFrom('test@gmail.com', 'Ruwan');
				$mail->addReplyTo('test@gmail.com', 'Ruwan');
				$mail->Subject = $EmailSchedule->email_subject;
				
				
				if(!is_null($request->attachment)){
			 			
				//$contents = Storage::get($uploadName);
 					$mail->AddAttachment($request->attachment->getPathName(),
                    $request->attachment->getClientOriginalName());
						 
						 
				}
		 

				$mail->SMTPSecure = 'tls';  
 				$mail->SMTPDebug = 0;  
				
				
				
				foreach ($gBatchImportsData as $row) {
  
									 try {
						$mail->addAddress($row->Email,$row->Name);
					} catch (Exception $e) {
						//echo 'Invalid address skipped: ' . htmlspecialchars($row->Email) . '<br>';
						continue;
					}
		 
					try {
						$mail->send();
					   // echo 'Message sent to :' . htmlspecialchars($row->Email) . ' (' . htmlspecialchars($row->Email) . ')<br>';
						//up  db
						
					} catch (Exception $e) {
			 
					 //   echo 'Mailer Error (' . htmlspecialchars($row->Email) . ') ' . $mail->ErrorInfo . '<br>';
 
						$mail->getSMTPInstance()->reset();
						
											
					}
 					$mail->clearAddresses();
					$mail->clearAttachments();
	 
	
				}
 				 
	 
			
		return back()->with('success','Saved Data, Mails Sent!');

				
				//return back()->with('success', 'Scheduled Successfully.');	
				
			}else {
				
				return back()->withErrors('Error', 'Error saving data');	

			}
			

    }
	
 

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('emailschedulemodule::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('emailschedulemodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
