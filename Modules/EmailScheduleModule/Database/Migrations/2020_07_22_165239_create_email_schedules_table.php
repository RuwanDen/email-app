<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_schedules', function (Blueprint $table) {
            $table->increments('id');
 			$table->string('email_alias',100);
			$table->string('email_subject',300);
			$table->text('email_body');
 			$table->string('attachment',100)->nullable();
 			$table->date('date_s');
 			$table->integer('user_batches_id');	
 			$table->integer('user_id');	
			$table->tinyInteger('defaultstatusis');	
            $table->timestamps();
        });
    } 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_schedules');
    }
}
