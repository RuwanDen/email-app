@extends('layouts.master')

@section('content')
 
 
	<div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Email Scheduler</h3>
  
  
 
		  
		  
        </div>
        <div class="box-body">




<div class="row">
        <!-- Left col -->
        <section class="col-lg-12  ">
		
		
					@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

			  @if($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
   
   
        <form role="form" method="post" enctype="multipart/form-data" action="{{ url('/submitEmailSchedule') }}">
    {{ csrf_field() }}
 

                <div class="box-body">
              <div class="form-group">
                <input class="form-control" placeholder="Email alias:" name="email_alias" required>
              </div>
			  
			                <div class="form-group">
                <input class="form-control" placeholder="Email Subject:" name="email_subject" required>
              </div>
		 
              <div class="form-group">
                    <textarea id="compose-textarea" class="form-control" style="height: 200px" name="email_body">
                    </textarea>
              </div>
			  
			  <div class="form-group">
                <label for="name">
                    <i class="fa fa-paperclip"></i> Attachment</label>
                <input type="file" class="form-control" id="attachment" name="attachment" >
            </div>
			
		 
			  
<div class="form-group">
                  <label>Select</label>
                  <select class="form-control" name="user_batches_id">
				  
			@foreach($data as $item)

                    <option value='{{ $item->id}}'> Batch-{{ $item->id}} </option>
 
			
 
			@endforeach
                
                  </select>
                </div>
				
				
				
				<div class="form-group">
                <label>Date:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" name="date_s">
                </div>
                <!-- /.input group -->
              </div>
			  
			  
			  
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                 <button type="submit" class="btn btn-primary"><i class="fa fa-envelope"></i> Send Emails</button>
              </div>
<input class="btn btn-danger" type="reset" value="Cancel">            </div>
            <!-- /.box-footer -->
          </div>

            </form>









        </div>
		
		
		
			
			
			
        <!-- /.box-body -->
        <div class="box-footer">
           
        </div>
        <!-- /.box-footer-->
      </div>
	  
	  
	   
	  
      <!-- /.row -->

      <!-- /.row (main row) -->
	  



@endsection