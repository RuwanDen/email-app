<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batch_imports', function (Blueprint $table) {
            $table->increments('id');  
 			$table->string('Name',500);
			$table->string('Number',20);
			$table->string('Email',200);
             $table->integer('row_id');
			    $table->integer('user_batches_id');				
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batch_imports');
    }
}
