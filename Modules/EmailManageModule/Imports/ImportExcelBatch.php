<?php

namespace Modules\EmailManageModule\Imports;

 use Modules\EmailManageModule\Entities\BatchImports;
use Maatwebsite\Excel\Concerns\ToModel;
 use Maatwebsite\Excel\Concerns\WithHeadingRow;


class ImportExcelBatch implements ToModel, WithHeadingRow
{
    /**
    * @return \Illuminate\Support\Collection
    */
 
		public $batchID=null;
 
 
		public function __construct($bid)
		{
			 $this->batchID=$bid;
		}
		
		
		public function model(array $row)
			{
	 
	// var_dump( $row);exit;
	    
						return new BatchImports([
				   'row_id'     => $row['id'],
				   'Name'     => $row['name'],
				   'Number'    => $row['number'],
				   'Email' =>  $row['email'] ,
				   'user_batches_id' =>$this->batchID,
				]);
			}
			
}
