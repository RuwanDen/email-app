@extends('layouts.master')

@section('content')
 

	<div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Email Manager</h3>
  
  
  <a href="{{ url('downloadFile') }}"  class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Excel file download
          </a>
		  
		  
        </div>
        <div class="box-body">




<div class="row">
        <!-- Left col -->
        <section class="col-lg-12  ">
       
 

          <!-- quick email widget -->
          <div >
            <div >
            </div>
			
			@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

			  @if($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
   
   
            <!-- /.box-header -->
            <!-- form start -->
 <form role="form" method="post" enctype="multipart/form-data" action="{{ url('/importExcelFile') }}">
    {{ csrf_field() }}

	<div class="box-body">
                           <h5 class="box-title">Upload File</h5>

                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" id="select_file" name="select_file">

                  <p class="help-block">Upload Pre-formatted excel file</p>
                </div>
				
	            
                <div class="form-group">
                 </div>

				
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
			  				<input class="btn btn-danger" type="reset" value="Cancel">

                <button type="submit" class="btn btn-primary">Submit</button>
				</div>
            </form>
          </div>

        </section>
   
      </div>












        </div>
        <!-- /.box-body -->
        <div class="box-footer">
           
        </div>
        <!-- /.box-footer-->
      </div>
	  
	  
	  
	  
	       <div class="box">
            <div class="box-header">
              <h3 class="box-title">User Batches</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="userbatch" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Batch ID</th>
                  <th>Date/Time </th>
                  <th>User ID</th>
             
                </tr>
                </thead>
                <tbody>
				
				 @foreach($data as $item)


				<tr>
					  <td> Batch-{{ $item->id}} </td>
					  <td>{{ $item->created_at}} </td>
					  <td> User-{{ $item->user_id}} </td>
					  
					 
                </tr>
            
			
 
				@endforeach


             
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
	  
      <!-- /.row -->

      <!-- /.row (main row) -->
@endsection