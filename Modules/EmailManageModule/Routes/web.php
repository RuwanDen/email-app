<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('EmailManage')->group(function() {
    Route::get('/', 'EmailManageModuleController@index');
});

Route::get('/downloadFile', 'EmailManageModuleExportController@export')->name('DownloadFile');
Route::post('/importExcelFile', 'ImportExcelFileController@import')->name('importExcelFile');

