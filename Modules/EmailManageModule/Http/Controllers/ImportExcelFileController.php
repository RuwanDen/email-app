<?php

namespace Modules\EmailManageModule\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Excel;
use Modules\EmailManageModule\Imports\ImportExcelBatch;
 use Modules\EmailManageModule\Entities\UserBatch as iUserBatch;

class ImportExcelFileController extends Controller
{    
  
    /**
     * Display a listing of the resource.
     * @return Response
     */
	public function __construct()
    {
        $this->middleware('auth');
    }
	 
    public function index()
    {
        return view('emailmanagemodule::index');
    }
	
	
	 public function import(Request $request)
    {
		
  		$validatedData = $request->validate([
		
			'select_file'  => 'required|mimes:xls,xlsx'

		]);
	  
			$UserBatch = new iUserBatch;
			$UserBatch->user_id = auth()->user()->id;
			$UserBatch->save();
			
		if($UserBatch){
			
			Excel::import(new ImportExcelBatch($UserBatch->id), $request->file('select_file')); 
			return back()->with('success', 'Excel Data File Imported successfully.');	
			
		} else {
			
			return back()->withErrors('Error', 'Error saving data');	

		}
 
 
	 
    }
	

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('emailmanagemodule::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('emailmanagemodule::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('emailmanagemodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
