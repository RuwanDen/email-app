<?php

namespace Modules\EmailManageModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\EmailManageModule\Entities\UserBatch as DataUserBatch;

class EmailManageModuleController extends Controller
{
	
		public function __construct()
		{
			$this->middleware('auth');
		}
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
		$getUserID=auth()->user()->id;
		    $data = DataUserBatch::where('user_id',$getUserID)
               ->orderBy('id', 'desc')
                ->get();

        return view('emailmanagemodule::index')->withData($data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('emailmanagemodule::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('emailmanagemodule::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('emailmanagemodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
