<?php

namespace Modules\EmailManageModule\Entities;

use Illuminate\Database\Eloquent\Model;

class UserBatch extends Model
{
    protected $fillable = [];
	
	public function batch_imports()
    {
        return $this->hasMany(BatchImports::class);
    }
	
	
}
