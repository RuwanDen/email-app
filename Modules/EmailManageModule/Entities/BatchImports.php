<?php

namespace Modules\EmailManageModule\Entities;

use Illuminate\Database\Eloquent\Model;

class BatchImports extends Model
{
	
		public $timestamps = false;

 	    protected $fillable = ['Name', 'Number', 'Email', 'row_id', 'user_batches_id'];

			
	public function user_batches()
    {
        return $this->belongsTo(UserBatch::class);
    }
	
	
}
