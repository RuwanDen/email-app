<?php

namespace App\Exports;

 use Maatwebsite\Excel\Concerns\WithHeadings;


class DownlodBlankExcelFile implements  WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
 

    public function headings(): array
    {
        return [
            'Id',
            'Name',
            'Number',
            'Email',
        ];
    }
	
}
