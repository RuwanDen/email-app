-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2020 at 05:50 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `emailapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `batch_imports`
--

CREATE TABLE `batch_imports` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `row_id` int(11) NOT NULL,
  `user_batches_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `batch_imports`
--

INSERT INTO `batch_imports` (`id`, `Name`, `Number`, `Email`, `row_id`, `user_batches_id`) VALUES
(1, 'Ruwan', '1', 'rr@ee.cc', 1, 22),
(2, 'tt', '1', 'rrw@ee.cc', 1, 22),
(3, 'gg', '1', 'rwwr@ee.cc', 1, 22),
(4, 'Ruwan', '1', 'rr@ee.cc', 1, 22),
(5, 'tt', '1', 'rrw@ee.cc', 1, 22),
(6, 'gg', '1', 'rwwr@ee.cc', 1, 22),
(7, 'Ruwan', '1', 'rr@ee.cc', 1, 22),
(8, 'tt', '1', 'rrw@ee.cc', 1, 22),
(9, 'gg', '1', 'rwwr@ee.cc', 1, 22),
(10, 'Ruwan', '1', 'rr@ee.cc', 1, 22),
(11, 'tt', '1', 'rrw@ee.cc', 1, 22),
(12, 'gg', '1', 'rwwr@ee.cc', 1, 22),
(13, 'sd', '234234', 'ff@hh.ll', 1, 22),
(14, 'sdf', '3423', '23@gg.kk', 2, 22),
(15, 'sd', '234234', 'dd@hh.ii', 3, 22),
(16, 'dfsdf', '423', 'ww@hh.uu', 4, 22),
(17, 'Ruwan', '1', 'rr@ee.cc', 1, 22),
(18, 'tt', '1', 'rrw@ee.cc', 1, 22),
(19, 'gg', '1', 'rwwr@ee.cc', 1, 22),
(20, 'Ruwan', '1', 'rr@ee.cc', 1, 22),
(21, 'tt', '1', 'rrw@ee.cc', 1, 22),
(22, 'gg', '1', 'rwwr@ee.cc', 1, 22),
(23, 'Ruwan', '1', 'rr@ee.cc', 1, 22),
(24, 'tt', '1', 'rrw@ee.cc', 1, 22),
(25, 'gg', '1', 'rwwr@ee.cc', 1, 22),
(26, 'Ruwan', '1', 'rr@ee.cc', 1, 10),
(27, 'tt', '1', 'rrw@ee.cc', 1, 10),
(28, 'gg', '1', 'rwwr@ee.cc', 1, 10),
(33, 'Ruwan', '1', 'rr@ee.cc', 1, 15),
(34, 'tt', '1', 'rrw@ee.cc', 1, 15),
(35, 'gg', '1', 'rwwr@ee.cc', 1, 15),
(36, 'dfsdf', '234', 'ruwan.web92@gmail.com\n', 33, 16),
(37, 'dfsdf', '234', 'ruwan.web92@gmail.com\n', 33, 17),
(38, 'd3323fsdf', '22234', 'ruwan.web92@gmail.com\n', 333, 17),
(39, 'g', '234', 'ruwan.web92@gmail.com\n', 3, 18),
(40, 'dfg', '234345', 'ruwan.web92@gmail.com\n', 433, 18);

-- --------------------------------------------------------

--
-- Table structure for table `email_schedules`
--

CREATE TABLE `email_schedules` (
  `id` int(10) UNSIGNED NOT NULL,
  `email_alias` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_subject` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_s` date NOT NULL,
  `user_batches_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `defaultstatusis` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_schedules`
--

INSERT INTO `email_schedules` (`id`, `email_alias`, `email_subject`, `email_body`, `attachment`, `date_s`, `user_batches_id`, `user_id`, `defaultstatusis`, `created_at`, `updated_at`) VALUES
(1, 'asdasd aw', 'asd awd ad', '<h1><u>Heading Of Message</u></h1>\r\n                       <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain\r\n                        was born and I will give you a complete account of the system, and expound the actual teachings\r\n                        of the great explorer of the truth, the master-builder of human happiness. No one rejects,\r\n                        dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know\r\n                        how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again\r\n                        is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain,\r\n                        but because occasionally circumstances occur in which toil and pain can procure him some great\r\n                        pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise,\r\n                        except to obtain some advantage from it? But who has any right to find fault with a man who\r\n                        chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that\r\n                        produces no resultant pleasure? On the other hand, we denounce with righteous indignation and\r\n                        dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so\r\n                        blinded by desire, that they cannot foresee</p>\r\n                 \r\n                      <p>Thank you,</p>\r\n                      <p>John Doe</p>', NULL, '2020-07-23', 1, 2, 1, '2020-07-22 16:50:09', '2020-07-22 16:50:09'),
(2, 'asdasd aw', 'asd awd ad', '<h1><u>Heading Of Message</u></h1>\r\n                       <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain\r\n                        was born and I will give you a complete account of the system, and expound the actual teachings\r\n                        of the great explorer of the truth, the master-builder of human happiness. No one rejects,\r\n                        dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know\r\n                        how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again\r\n                        is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain,\r\n                        but because occasionally circumstances occur in which toil and pain can procure him some great\r\n                        pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise,\r\n                        except to obtain some advantage from it? But who has any right to find fault with a man who\r\n                        chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that\r\n                        produces no resultant pleasure? On the other hand, we denounce with righteous indignation and\r\n                        dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so\r\n                        blinded by desire, that they cannot foresee</p>\r\n                 \r\n                      <p>Thank you,</p>\r\n                      <p>John Doe</p>', NULL, '2020-07-23', 1, 2, 1, '2020-07-22 17:36:19', '2020-07-22 17:36:19'),
(3, 'asdasd aw', 'asd awd ad', '<h1><u>Heading Of Message</u></h1>\r\n                       <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain\r\n                        was born and I will give you a complete account of the system, and expound the actual teachings\r\n                        of the great explorer of the truth, the master-builder of human happiness. No one rejects,\r\n                        dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know\r\n                        how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again\r\n                        is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain,\r\n                        but because occasionally circumstances occur in which toil and pain can procure him some great\r\n                        pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise,\r\n                        except to obtain some advantage from it? But who has any right to find fault with a man who\r\n                        chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that\r\n                        produces no resultant pleasure? On the other hand, we denounce with righteous indignation and\r\n                        dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so\r\n                        blinded by desire, that they cannot foresee</p>\r\n                 \r\n                      <p>Thank you,</p>\r\n                      <p>John Doe</p>', NULL, '2020-07-23', 1, 2, 1, '2020-07-22 17:51:08', '2020-07-22 17:51:08'),
(4, 'asdasd aw', 'asd awd ad', '<h1><u>Heading Of Message</u></h1>\r\n                       <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain\r\n                        was born and I will give you a complete account of the system, and expound the actual teachings\r\n                        of the great explorer of the truth, the master-builder of human happiness. No one rejects,\r\n                        dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know\r\n                        how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again\r\n                        is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain,\r\n                        but because occasionally circumstances occur in which toil and pain can procure him some great\r\n                        pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise,\r\n                        except to obtain some advantage from it? But who has any right to find fault with a man who\r\n                        chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that\r\n                        produces no resultant pleasure? On the other hand, we denounce with righteous indignation and\r\n                        dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so\r\n                        blinded by desire, that they cannot foresee</p>\r\n                 \r\n                      <p>Thank you,</p>\r\n                      <p>John Doe</p>', '2020-07-22-23-29-00', '2020-07-23', 1, 2, 1, '2020-07-22 17:59:00', '2020-07-22 17:59:00'),
(5, 'asdasd aw', 'asd awd ad', '<h1><u>Heading Of Message</u></h1>\r\n                       <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain\r\n                        was born and I will give you a complete account of the system, and expound the actual teachings\r\n                        of the great explorer of the truth, the master-builder of human happiness. No one rejects,\r\n                        dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know\r\n                        how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again\r\n                        is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain,\r\n                        but because occasionally circumstances occur in which toil and pain can procure him some great\r\n                        pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise,\r\n                        except to obtain some advantage from it? But who has any right to find fault with a man who\r\n                        chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that\r\n                        produces no resultant pleasure? On the other hand, we denounce with righteous indignation and\r\n                        dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so\r\n                        blinded by desire, that they cannot foresee</p>\r\n                 \r\n                      <p>Thank you,</p>\r\n                      <p>John Doe</p>', '2020-07-22-23-30-23', '2020-07-23', 1, 2, 1, '2020-07-22 18:00:23', '2020-07-22 18:00:23'),
(6, 'asdasd aw', 'asd awd ad', '<h1><u>Heading Of Message</u></h1>\r\n                       <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain\r\n                        was born and I will give you a complete account of the system, and expound the actual teachings\r\n                        of the great explorer of the truth, the master-builder of human happiness. No one rejects,\r\n                        dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know\r\n                        how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again\r\n                        is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain,\r\n                        but because occasionally circumstances occur in which toil and pain can procure him some great\r\n                        pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise,\r\n                        except to obtain some advantage from it? But who has any right to find fault with a man who\r\n                        chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that\r\n                        produces no resultant pleasure? On the other hand, we denounce with righteous indignation and\r\n                        dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so\r\n                        blinded by desire, that they cannot foresee</p>\r\n                 \r\n                      <p>Thank you,</p>\r\n                      <p>John Doe</p>', '2020-07-22-23-31-45ClocksetsReport2020-07-17_484.csv', '2020-07-23', 1, 2, 1, '2020-07-22 18:01:45', '2020-07-22 18:01:45'),
(7, 'asdasd aw', 'asd awd ad', '<h1><u>Heading Of Message</u></h1>\r\n                       <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain\r\n                        was born and I will give you a complete account of the system, and expound the actual teachings\r\n                        of the great explorer of the truth, the master-builder of human happiness. No one rejects,\r\n                        dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know\r\n                        how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again\r\n                        is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain,\r\n                        but because occasionally circumstances occur in which toil and pain can procure him some great\r\n                        pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise,\r\n                        except to obtain some advantage from it? But who has any right to find fault with a man who\r\n                        chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that\r\n                        produces no resultant pleasure? On the other hand, we denounce with righteous indignation and\r\n                        dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so\r\n                        blinded by desire, that they cannot foresee</p>\r\n                 \r\n                      <p>Thank you,</p>\r\n                      <p>John Doe</p>', '2020-07-23-00-01-43EmailExcel (8).xlsx', '2020-07-23', 3, 1, 1, '2020-07-22 18:31:44', '2020-07-22 18:31:44'),
(8, 'asdasd aw', 'asd awd ad', '<p>f sdf gdf sdf g</p>', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 19:42:19', '2020-07-22 19:42:19'),
(9, 'asdasd aw', 'asd awd ad', '<p>test set</p>', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 19:43:10', '2020-07-22 19:43:10'),
(10, 'asdasd aw', 'asd awd ad', '<p>sdfsd sdf&nbsp;<br></p>', '2020-07-23-01-14-25sri-lanka-nature-beach-waves-wallpaper-preview.jpg', '2020-07-23', 8, 1, 1, '2020-07-22 19:44:26', '2020-07-22 19:44:26'),
(11, 'asdasd aw', 'asd awd ad', '<p>zczc</p>', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 19:45:11', '2020-07-22 19:45:11'),
(12, 'asdasd aw', 'asd awd ad', '<p>asdasd</p>', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 19:45:22', '2020-07-22 19:45:22'),
(13, 'asdasd aw', 'asd', '<p>asd</p>', NULL, '2020-07-23', 4, 1, 1, '2020-07-22 19:45:35', '2020-07-22 19:45:35'),
(14, 'asdasd aw', 'asd awd ad', '<p>sdf sdf</p>', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 19:48:51', '2020-07-22 19:48:51'),
(15, 'asdasd aw', 'asd awd ad', '<p>sdfsdf</p>', '2020-07-23-01-19-33r-cv.pdf', '2020-07-23', 8, 1, 1, '2020-07-22 19:49:33', '2020-07-22 19:49:33'),
(16, 'asdasd aw', 'asd awd ad', '<p>sdfsdfsdfsdf</p>', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 19:50:11', '2020-07-22 19:50:11'),
(17, 'asdasd aw', 'asd', '<p>aedfadf</p>', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 19:56:09', '2020-07-22 19:56:09'),
(18, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:03:22', '2020-07-22 20:03:22'),
(19, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:04:50', '2020-07-22 20:04:50'),
(20, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:05:44', '2020-07-22 20:05:44'),
(21, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:06:47', '2020-07-22 20:06:47'),
(22, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:07:08', '2020-07-22 20:07:08'),
(23, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:07:33', '2020-07-22 20:07:33'),
(24, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:07:46', '2020-07-22 20:07:46'),
(25, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:10:05', '2020-07-22 20:10:05'),
(26, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:10:25', '2020-07-22 20:10:25'),
(27, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:12:19', '2020-07-22 20:12:19'),
(28, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:13:26', '2020-07-22 20:13:26'),
(29, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:14:03', '2020-07-22 20:14:03'),
(30, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:14:11', '2020-07-22 20:14:11'),
(31, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:14:17', '2020-07-22 20:14:17'),
(32, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:15:08', '2020-07-22 20:15:08'),
(33, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:16:10', '2020-07-22 20:16:10'),
(34, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 8, 1, 1, '2020-07-22 20:16:37', '2020-07-22 20:16:37'),
(35, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 16, 2, 1, '2020-07-22 20:49:17', '2020-07-22 20:49:17'),
(36, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 16, 2, 1, '2020-07-22 20:49:53', '2020-07-22 20:49:53'),
(37, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 16, 2, 1, '2020-07-22 20:50:52', '2020-07-22 20:50:52'),
(38, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 16, 2, 1, '2020-07-22 20:57:21', '2020-07-22 20:57:21'),
(39, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 16, 2, 1, '2020-07-22 20:59:03', '2020-07-22 20:59:03'),
(40, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 16, 2, 1, '2020-07-22 20:59:42', '2020-07-22 20:59:42'),
(41, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 16, 2, 1, '2020-07-22 21:01:09', '2020-07-22 21:01:09'),
(42, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 16, 2, 1, '2020-07-22 21:01:34', '2020-07-22 21:01:34'),
(43, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 16, 2, 1, '2020-07-22 21:01:59', '2020-07-22 21:01:59'),
(44, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 16, 2, 1, '2020-07-22 21:03:27', '2020-07-22 21:03:27'),
(45, 'Email Scheduler', 'asd asd Email Scheduler', 'dfg dfg df gdg sdf gdfg&nbsp;\r\n\r\n<h2></h2><h2>Email Scheduler</h2><h2></h2><h2></h2><h2></h2><h2></h2>Bodyyy&nbsp;', NULL, '2020-07-23', 16, 2, 1, '2020-07-22 21:07:16', '2020-07-22 21:07:16'),
(46, 'Email Scheduler', 'asd asd Email Scheduler', 'dfg dfg df gdg sdf gdfg&nbsp;\r\n\r\n<h2></h2><h2>Email Scheduler</h2><h2></h2><h2></h2><h2></h2><h2></h2>Bodyyy&nbsp;', NULL, '2020-07-23', 16, 2, 1, '2020-07-22 21:15:33', '2020-07-22 21:15:33'),
(47, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 16, 2, 1, '2020-07-22 21:43:10', '2020-07-22 21:43:10'),
(48, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', '2020-07-23-03-22-3637748540_419593885201202_3518852570875428864_n (1).jpg', '2020-07-23', 16, 2, 1, '2020-07-22 21:52:36', '2020-07-22 21:52:36'),
(49, 'asd asd asd withou t attachh', 'asd asd asd withou t attachh', 'dfg dfg df gdg sdf gdfg withou t attachh withou t attachh', NULL, '2020-07-23', 16, 2, 1, '2020-07-22 21:53:17', '2020-07-22 21:53:17'),
(50, 'asd asd asd', 'asd asd asd', 'dfg dfg df gdg sdf gdfg', NULL, '2020-07-23', 17, 2, 1, '2020-07-22 21:56:35', '2020-07-22 21:56:35'),
(51, 'Email Scheduler', 'asd asd Email Scheduler', '<p>testt</p>', '2020-07-23-03-43-1637748540_419593885201202_3518852570875428864_n (1).jpg', '2020-07-23', 18, 2, 1, '2020-07-22 22:13:16', '2020-07-22 22:13:16'),
(52, 'Email Scheduler', 'asd awd ad', '<p>sd</p>', NULL, '2020-07-23', 18, 2, 1, '2020-07-22 22:14:38', '2020-07-22 22:14:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_07_22_080658_create_batch_imports_table', 1),
(2, '2020_07_22_080748_create_user_batches_table', 1),
(11, '2020_07_22_165239_create_email_schedules_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ruwan@ruwan.com', 'ruwan@ruwan.com', '$2y$10$HMalLArg5O8SnJIkjJrp/.UHazg39BX/4MMKPlfISK891qNxzU9pe', 'rtlyiNqtpnvDmtCjTXogIql3nHSUQQ4NC4bEJL1bOJDMnZjGlzcGP9pSuWCQ', '2020-07-21 11:12:43', '2020-07-21 11:12:43'),
(2, 'thilak@thilak.com', 'thilak@thilak.com', '$2y$10$/EREDEoR9FWi7mM.d9ykVuImHvxQNuntPzFr8oeu/OcrlU89zSKBC', 'lkCRn5c8f9CVOkJx7RcQN7giBpnXdsni3VRrY4e7MwVPZvWpTAJGh6ipyXy6', '2020-07-22 09:17:56', '2020-07-22 09:17:56');

-- --------------------------------------------------------

--
-- Table structure for table `user_batches`
--

CREATE TABLE `user_batches` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_batches`
--

INSERT INTO `user_batches` (`id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-07-22 07:12:38', '2020-07-22 07:12:38'),
(2, 1, '2020-07-22 07:14:09', '2020-07-22 07:14:09'),
(3, 1, '2020-07-22 07:33:33', '2020-07-22 07:33:33'),
(4, 1, '2020-07-22 07:34:02', '2020-07-22 07:34:02'),
(5, 1, '2020-07-22 08:58:07', '2020-07-22 08:58:07'),
(6, 1, '2020-07-22 09:01:13', '2020-07-22 09:01:13'),
(7, 2, '2020-07-22 09:18:23', '2020-07-22 09:18:23'),
(8, 1, '2020-07-22 18:44:26', '2020-07-22 18:44:26'),
(9, 1, '2020-07-22 20:24:44', '2020-07-22 20:24:44'),
(10, 1, '2020-07-22 20:26:40', '2020-07-22 20:26:40'),
(11, 2, '2020-07-22 20:29:36', '2020-07-22 20:29:36'),
(12, 2, '2020-07-22 20:30:47', '2020-07-22 20:30:47'),
(13, 2, '2020-07-22 20:31:09', '2020-07-22 20:31:09'),
(14, 2, '2020-07-22 20:32:43', '2020-07-22 20:32:43'),
(15, 2, '2020-07-22 20:32:59', '2020-07-22 20:32:59'),
(16, 2, '2020-07-22 20:34:43', '2020-07-22 20:34:43'),
(17, 2, '2020-07-22 21:56:15', '2020-07-22 21:56:15'),
(18, 2, '2020-07-22 22:12:35', '2020-07-22 22:12:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `batch_imports`
--
ALTER TABLE `batch_imports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_schedules`
--
ALTER TABLE `email_schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_batches`
--
ALTER TABLE `user_batches`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `batch_imports`
--
ALTER TABLE `batch_imports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `email_schedules`
--
ALTER TABLE `email_schedules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_batches`
--
ALTER TABLE `user_batches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
